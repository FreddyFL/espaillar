<br>
<section class="context-dark" style="margin-top: -217px;">
  <div class="shell section-98 section-sm-110">
    <h2 class="text-bold" style="color:#15879A;font-weight: 500;">¿TU CASA ENAMORA? ¿Y SI TU INMUEBLE CAPTASE TODAS LAS MIRADAS?</h2>
      <h2 class="text-bold" style="color:#15879A;font-weight: 500;">Preparamos tu vivienda para una venta c&oacute;moda, f&aacute;cil y r&aacute;pida.</h2>
      <h2 style="text-align: center; font-size:44; line-height: 1.1;" data-fontsize="44" data-lineheight="48.4px" class="fusion-responsive-typography-calculated"><span style="color:#000;">Quiero saber más<br>
         </span>
      </h2>
      <div class="text-center">
        <style>.fusion-button.button-1 .fusion-button-text,.fusion-button.button-1 i{color:#15879A;}.fusion-button.button-1{border-color:#15879A;border-radius:0px 0px 0px 0px;background:rgba(255,255,255,0);}.fusion-button.button-1:hover .fusion-button-text,.fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text,.fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text,.fusion-button.button-1:active i{color:#ffffff;}.fusion-button.button-1:hover,.fusion-button.button-1:active,.fusion-button.button-1:focus{border-color:#15879A;background:#15879A;}
        </style>
        <a class="fusion-button button-flat button-xlarge button-custom button-1 fusion-button-default-span fusion-button-default-type" target="_blank" href="{{route('contactos')}}"><i class="mdi mdi-cursor-pointer button-icon-left" aria-hidden="true"></i><span class="fusion-button-text">Contacta con nosotros</span>
        </a>
      </div>
      <br>
      <hr style="border-top: 4px solid #747474;opacity: 0.4;">
  </div>
</section>
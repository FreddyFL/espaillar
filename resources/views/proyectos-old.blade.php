<!DOCTYPE html>
<html class="wide wow-animation smoothscroll scrollTo" lang="en">
  <head>
    <!-- Site Title-->
    <title>ESPAIL-LLAR</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="Real Estate web design multipurpose template">
    <meta name="date" content="Dec 26">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat:400,700%7CLato:400,700'">
    <link rel="stylesheet" href="css/style.css">
		 <style>
      .fullscreen-modal .modal-dialog {
        margin: 0;
        margin-right: auto;
        margin-left: auto;
        width: 100%;
      }
      .pc{
        display: block;
      }
      .mobile{
          display: none;
      } 
      @media (min-width: 540px) {
        .pc{
        display: none;
      }
      .mobile{
          display: block;
      } 
      }
      /*  .imagenes{
        width: 570;
        margin-right: 0px;
      }
      
      
  
       @media only screen and (max-width: 320px) {
        .imagenes{
          width: 300;
        }
      }
     @media only screen and (max-width: 360px) {
        .imagenes{
          width: 320;
        }
      } 
      @media only screen and (max-width: 480px) {
        .imagenes{
          width: 360;
        }
      }
       @media only screen and (max-width: 540px) {
        .pc{
          display: none;
        }
        .mobile{
          display: block;
        } 
        .imagenes{
          width: 400;
        }
      }
      @media only screen and (max-width: 1025px) {
        .pc{
          display: none;
        }
        .mobile{
          display: block;
        } 
        .imagenes{
          width: 400;
        }
      }*/
      @media (min-width: 768px) {
        .fullscreen-modal .modal-dialog {
          width: 750px;
        }
      }
      @media (min-width: 992px) {
        .fullscreen-modal .modal-dialog {
          width: auto;
        }
      }
      @media (min-width: 1200px) {
        .fullscreen-modal .modal-dialog {
           width: auto;
        }
      }
      @media (min-width: 1920px) {
        .fullscreen-modal .modal-dialog {
           width: auto;
        }
      }/*
      .fullscreen-modal .modal-dialog {
        margin: 0;
        margin-right: auto;
        margin-left: auto;
        width: 100%;
       
      }
      .pc{
        display: block;
      }
      .mobile{
          display: none;
      } 
      .imagenes{
        width: 570;
        margin-right: 0px;
      }
      @media only screen and (max-width: 540px) {
        .pc{
          display: none;
        }
        .mobile{
          display: block;
        } 
        .imagenes{
          width: 400;
        }
      }
      @media only screen and (max-width: 480px) {
        .imagenes{
          width: 360;
        }
      }
      @media only screen and (max-width: 360px) {
        .imagenes{
          width: 320;
        }
      } 
      @media only screen and (max-width: 320px) {
        .imagenes{
          width: 300;
        }
      } 
      @media (min-width: 768px) {
        .fullscreen-modal .modal-dialog {
          width: 750px;
        }
      }
      @media (min-width: 992px) {
        .fullscreen-modal .modal-dialog {
          width: auto;
        }
      }
      @media (min-width: 1200px) {
        .fullscreen-modal .modal-dialog {
           width: auto;
        }
      }
      @media (min-width: 1920px) {
        .fullscreen-modal .modal-dialog {
           width: auto;
        }
      }
      */
     </style> 
  </head>
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Head-->
      <header class="page-head">
        <!-- RD Navbar Transparent-->
       @include('navbar-proyectos')
        <section class="context-dark">
          <div class="parallax-container" >
            <div >
              <div class="shell section-top-34 section-sm-top-98 section-bottom-34">
                <div>
                  <h1 class="text-bold" style="color: #15879A;">ALGUNOS DE NUESTROS  PROYECTOS</h1>
                </div>
              
              </div>
            </div>
          </div>
        </section>
      </header>
      <!-- Page Contents-->
      <main class="page-content">
        <!--Section Property Filter-->
      
        <!--Section Properties-->
        <section class="section-66 section-sm-bottom-110">
          <div class="shell">
            <div class="range range-xs-center">
              <div class="cell-md-8">
                <div class="range">
                  <div class="cell-sm-4 offset-top-66">
                      <h5 class="text-bold text-primary"><a data-toggle="modal" data-target="#exampleModal">AVA Nob Hill</a></h5>
                   
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <button type="button"  data-toggle="modal" data-target="#exampleModal">
                          <img class="img-responsive reveal-inline-block" src="images/home-img-05-370x250.jpg" width="370" height="250" alt="">
                        </button>
                      </div>
                      
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-66 ">
                    <h5 class="text-bold text-primary"><a data-toggle="modal" data-target="#exampleModal">AVA Nob Hill</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <button type="button"  data-toggle="modal" data-target="#exampleModal">
                        <img class="img-responsive reveal-inline-block" src="images/home-img-06-370x250.jpg" width="370" height="250" alt="">
                        </button>
                      </div>
                      
                    </div>
                  </div>
                   <div class="cell-sm-4 offset-top-66">
                      <h5 class="text-bold text-primary"><a data-toggle="modal" data-target="#exampleModal">AVA Nob Hill</a></h5>
                      
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <button type="button"  data-toggle="modal" data-target="#exampleModal">
                        <img class="img-responsive reveal-inline-block" src="images/home-img-07-370x250.jpg" width="370" height="250" alt="">
                        </button>
                      </div>
                     
                    </div>
                  </div>
                   <div class="cell-sm-4 offset-top-66">
                    <h5 class="text-bold text-primary"><a data-toggle="modal" data-target="#exampleModal">The Presidio Residences</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <button type="button"  data-toggle="modal" data-target="#exampleModal">
                        
                        <img class="img-responsive reveal-inline-block" src="images/home-img-07-370x250.jpg" width="370" height="250" alt="">
                          </button>
                      </div>
                      
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-66">
                    <h5 class="text-bold text-primary"><a data-toggle="modal" data-target="#exampleModal">The Presidio Residences</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <button type="button"  data-toggle="modal" data-target="#exampleModal">
                        <img class="img-responsive reveal-inline-block" src="images/home-img-07-370x250.jpg" width="370" height="250" alt="">
                            </button>
                      </div>
                     
                    </div>
                  </div>
                   <div class="cell-sm-4 offset-top-66">
                    <h5 class="text-bold text-primary"><a data-toggle="modal" data-target="#exampleModal">The Presidio Residences</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                         <button type="button"  data-toggle="modal" data-target="#exampleModal">
                      
                        <img class="img-responsive reveal-inline-block" src="images/home-img-07-370x250.jpg" width="370" height="250" alt="">
                           </button>
                      </div>
                      
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-66">
                    <h5 class="text-bold text-primary"><a data-toggle="modal" data-target="#exampleModal">South Bay Residence</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <button type="button"  data-toggle="modal" data-target="#exampleModal">
                      
                        <img class="img-responsive reveal-inline-block" src="images/home-img-08-370x250.jpg" width="370" height="250" alt=""></button>
                      </div>
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-66">
                      <h5 class="text-bold text-primary"><a data-toggle="modal" data-target="#exampleModal">Avalon at Mission Bay</a></h5> 
                      <div class="text-sm-left offset-top-24">
                      <div>
                        <button type="button"  data-toggle="modal" data-target="#exampleModal">
                      
                         <img class="img-responsive reveal-inline-block" src="images/home-img-09-370x250.jpg" width="370" height="250" alt=""></button>
                      </div>
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-66">
                    <h5 class="text-bold text-primary"><a data-toggle="modal" data-target="#exampleModal">Bayside Village Apartments</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <button type="button"  data-toggle="modal" data-target="#exampleModal">
                      
                        <img class="img-responsive reveal-inline-block" src="images/home-img-10-370x250.jpg" width="370" height="250" alt=""></button>
                      </div>
                    </div>
                  </div>
                </div>
               
              </div>
              
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
      <!-- Default footer-->
      <footer class="section-relative section-top-66 section-bottom-34 page-footer bg-gray-base context-dark">
        <div class="shell">
          <div class="range range-sm-center text-lg-left">
            <div class="cell-sm-8 cell-md-12">
              <div class="range range-xs-center">
                <div class="cell-xs-7 text-xs-left cell-md-4 cell-lg-3 cell-lg-push-4">
                  <h6 class="text-uppercase text-spacing-60">Latest news</h6>
                        <!-- Post Widget-->
                        <article class="post widget-post text-left"><a href="#">
                            <div class="unit unit-horizontal unit-spacing-xs unit-middle">
                              <div class="unit-body">
                                <div class="post-meta"><span class="icon-xxs text-primary mdi mdi-arrow-right"></span>
                                  <time class="text-dark" datetime="2017-01-01">05/14/2017</time>
                                </div>
                                <div class="post-title">
                                  <h6 class="text-regular">Top 10 Apartments in LA</h6>
                                </div>
                              </div>
                            </div></a></article>
                        <!-- Post Widget-->
                        <article class="post widget-post text-left"><a href="#">
                            <div class="unit unit-horizontal unit-spacing-xs unit-middle">
                              <div class="unit-body">
                                <div class="post-meta"><span class="icon-xxs text-primary mdi mdi-arrow-right"></span>
                                  <time class="text-dark" datetime="2017-01-01">05/14/2017</time>
                                </div>
                                <div class="post-title">
                                  <h6 class="text-regular">Choosing a Rental Property</h6>
                                </div>
                              </div>
                            </div></a></article>
                        <!-- Post Widget-->
                        <article class="post widget-post text-left"><a href="#">
                            <div class="unit unit-horizontal unit-spacing-xs unit-middle">
                              <div class="unit-body">
                                <div class="post-meta"><span class="icon-xxs text-primary mdi mdi-arrow-right"></span>
                                  <time class="text-dark" datetime="2017-01-01">05/14/2017</time>
                                </div>
                                <div class="post-title">
                                  <h6 class="text-regular">Features of Mortgage Loans</h6>
                                </div>
                              </div>
                            </div></a></article>
                </div>
                <div class="cell-xs-5 offset-top-41 offset-xs-top-0 text-xs-left cell-md-3 cell-lg-2 cell-lg-push-3">
                  <h6 class="text-uppercase text-spacing-60">Useful Links</h6>
                  <div class="reveal-block">
                    <div class="reveal-inline-block">
                      <ul class="list list-marked">
                        <li><a href="#">Properties</a></li>
                        <li><a href="#">Amenities</a></li>
                        <li><a href="#">Rentals</a></li>
                        <li><a href="#">Mortgages</a></li>
                        <li><a href="#">Agents</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="cell-xs-12 offset-top-41 cell-md-5 offset-md-top-0 text-md-left cell-lg-4 cell-lg-push-2">
                  <h6 class="text-uppercase text-spacing-60">Newsletter</h6>
                  <p>Keep up with the latest company news and events. Enter your e-mail and subscribe to our newsletter.</p>
                  <div class="offset-top-30">
                          <form class="rd-mailform" data-form-output="form-subscribe-footer" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                            <div class="form-group">
                              <div class="input-group input-group-sm"><span class="input-group-addon"><span class="input-group-icon mdi mdi-email"></span></span>
                                <input class="form-control" placeholder="Type your E-Mail" type="email" name="email" data-constraints="@Required @Email"><span class="input-group-btn">
                                  <button class="btn btn-sm btn-primary" type="submit">Subscribe</button></span>
                              </div>
                            </div>
                            <div class="form-output" id="form-subscribe-footer"></div>
                          </form>
                  </div>
                </div>
                <div class="cell-xs-12 offset-top-66 cell-lg-3 cell-lg-push-1 offset-lg-top-0">
                  <!-- Footer brand-->
                  <div class="footer-brand"><a href="index.html"><img width='84' height='50' src='images/logo-light.png' alt=''/></a></div>
                  <p class="text-darker offset-top-4 inset-right-15 inset-lg-right-0 inset-lg-left-15"></p>
                        <ul class="list-inline">
                          <li><a class="icon fa fa-facebook icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                          <li><a class="icon fa fa-twitter icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                          <li><a class="icon fa fa-google-plus icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                          <li><a class="icon fa fa-linkedin icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                        </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="shell offset-top-50">
          <p class="small text-darker">Real Estate &copy; <span id="copyright-year"></span> . <a href="privacy.html">Privacy Policy</a></p>
        </div>
      </footer>
      <!-- Modal -->
<div class="modal fullscreen-modal  fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" 
style="margin-top: 10px; display: block; z-index: 99999;    position: absolute;">
  <div class="modal-dialog">
    <div class="modal-content" style="border-radius: 0;background-clip: border-box;">
      <div class="modal-header" style="border-bottom: 1px solid #fff;">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #15879A;opacity: 1;">
          <span aria-hidden="true" style="color: #15879A;">&times;</span>
        </button>
      </div>
      <div class="modal-body pc" style="padding: 0px;">
        <section class="section-98 section-sm-110">
          <div class="shell">
            <h2 class="text-bold">The Presidio Residences</h2>
            <hr class="divider bg-saffron">
            <div class="offset-sm-top-66">
              <div class="range">
                 <div class="cell-md-6 cell-lg-6 cell-xl-6">
                  <div class="text-sm-left offset-top-50">
                   <h5 class="text-bold">Description</h5>
                    <p>The Presidio Residences offer a unique opportunity to live in a national park setting and enjoy quiet neighborhoods, convenient location, beautiful open spaces, and outstanding recreational amenities that you won’t find anywhere else in the area.</p>
                    <p>This property includes a wide variety of facilities that can make your dwelling in the picturesque area of Southern Los Angeles very comfortable. There is everything modern citizen may need, even the most up-to-date technologies that are organically integrated with all the communications of the house. If you are looking for a calm place to live, the Presidential Residences have something to offer you. With effective planning and additional amenities available as a part of the property, these apartments can give you an unprecedented level of comfort with a quick access to the airport, train station, and the city center.</p>
                  </div>
                  </div>
                <div class="cell-md-6 cell-lg-6 cell-xl-6">
                  <!-- Owl Carousel-->
                  <div class="owl-carousel owl-carousel-classic" data-items="1" data-dots="true" data-nav="true" data-autoplay="true" data-loop="true" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]" data-photo-swipe-gallery="" style="width: 570px;margin-right: 0px;">
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-01-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-01-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-02-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-02-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-03-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-03-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-05-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-05-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                   <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-06-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-06-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                      <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-07-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-07-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-08-770x510.jpg">
                    <figure><img width="770" height="510" src="images/single-property-page-08-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                  </div>
                   </div>

                 
                 </div>
            </div>
          </div>
        </section>
      </div>
       <div class="modal-body mobile" style="padding: 0px;">
        <section class="section-98 section-sm-110">
          <div class="shell">
            <h2 class="text-bold">The Presidio Residences</h2>
            <hr class="divider bg-saffron">
            <div class="offset-sm-top-66">
              <div class="range">
                 
                <div class="cell-md-6 cell-lg-6 cell-xl-6">
                  <!-- Owl Carousel-->
                  <div class="owl-carousel owl-carousel-classic" data-items="1" data-dots="true" data-nav="true" data-autoplay="true" data-loop="true" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]" data-photo-swipe-gallery="" style="width: 570px;margin-right: 0px;">
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-01-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-01-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-02-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-02-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-03-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-03-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-05-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-05-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                   <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-06-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-06-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                      <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-07-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-07-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-08-770x510.jpg">
                    <figure><img width="770" height="510" src="images/single-property-page-08-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                  </div>
                   </div>
<div class="cell-md-6 cell-lg-6 cell-xl-6">
                  <div class="text-sm-left offset-top-50">
                   <h5 class="text-bold">Description</h5>
                    <p>The Presidio Residences offer a unique opportunity to live in a national park setting and enjoy quiet neighborhoods, convenient location, beautiful open spaces, and outstanding recreational amenities that you won’t find anywhere else in the area.</p>
                    <p>This property includes a wide variety of facilities that can make your dwelling in the picturesque area of Southern Los Angeles very comfortable. There is everything modern citizen may need, even the most up-to-date technologies that are organically integrated with all the communications of the house. If you are looking for a calm place to live, the Presidential Residences have something to offer you. With effective planning and additional amenities available as a part of the property, these apartments can give you an unprecedented level of comfort with a quick access to the airport, train station, and the city center.</p>
                  </div>
                  </div>
                 
                 </div>
            </div>
          </div>
        </section>
      </div>
   {{--   <div class="modal-footer" style="border-top: 1px solid #fff;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>--}}
    </div>
  </div>
</div>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- PhotoSwipe Gallery-->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>
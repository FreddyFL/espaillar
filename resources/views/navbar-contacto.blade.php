<div class="rd-navbar-wrap" style="height:auto !important">
  <nav class="rd-navbar rd-navbar-top-panel rd-navbar-light" data-lg-stick-up-offset="79px" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-lg-auto-height="true" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
    <div class="rd-navbar-inner" style="padding: 30px 15px; top:0 !important;">
      <div class="nav-content">
        <div  class="nav-menu">
          <!-- RD Navbar Panel-->
          <div class="rd-navbar-panel">
            <!-- RD Navbar Toggle-->
            <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap"><span></span></button>
            <!-- RD Navbar Top Panel Toggle-->
            <button class="rd-navbar-top-panel-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-top-panel"><span></span></button>
            <!--Navbar Brand-->
            <div class="rd-navbar-brand"><a href="./"><img  height='50' src='images/logo.png' alt=''/></a></div>
          </div>
          <div class="rd-navbar-menu-wrap content-aling-center">
            <div class="rd-navbar-nav-wrap">
              <div class="rd-navbar-mobile-scroll">
                <!--Navbar Brand Mobile-->
                <div class="rd-navbar-mobile-brand"><a href="./"><img  height='50' src='images/logo.png' alt=''/></a></div>
                <!-- RD Navbar Nav-->
                <ul class="rd-navbar-nav">
                   <li ><a href="{{url('/')}}"><span>Inicio</span></a>
                        </li>
                        <li ><a href="{{route('proyectos')}}"><span>Proyectos</span></a>
                        </li>
                       
                        <li class="active"><a href="#"><span>Contacto</span></a>
                        </li>
                  <li class="list-inline-item"><a class="fa fa-instagram icono-tam" href="https://www.instagram.com/espail.llar/" target="_blank" style="color:#000;    font-size: 30px;"></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
         <div class="rd-navbar-top-panel">
          <div class="nav-contact"> 
            <div>
              <address class="contact-info d-md-inline-block text-left offset-top-10 offset-md-top-0" >
                <div class="p content-aling-center" >
                  <div class="unit-left">
                    <span class="mdi mdi-whatsapp" style="color: #15879A;  margin-right: 0.6em;"></span>
                  </div>
                  <div class="unit-body">
                    <a class="text-gray-darker" href="intent://send/+34628015890#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end" style="text-decoration: none;">628 01 58 90</a>
                  </div>
                </div>
              </address>
            </div>
            <div>
              <address class="contact-info d-md-inline-block text-left offset-top-10 offset-md-top-0">
                <div class="p content-aling-center" >
                  <div class="unit-left">
                    <span class="gi gi-earphone  mdi mdi-phone" style="color: #15879A;  margin-right: 0.6em;"></span>
                  </div>
                  <div class="unit-body">
                    <a class="text-gray-darker" href="tel:+34977590159" style="text-decoration: none;">977 59 01 59</a>
                  </div>
                </div>
              </address>
            </div>
          </div>  
        </div>
      </div>
    </div>
 </nav>
</div>
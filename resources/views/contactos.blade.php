<!DOCTYPE html>
<html class="wide wow-animation smoothscroll scrollTo" lang="en">
  <head>
    <!-- Site Title-->
    <title>Contacto</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="Real Estate web design multipurpose template">
    <meta name="date" content="Dec 26">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat:400,700%7CLato:400,700'">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
     <link rel="stylesheet" href="{{ asset('css/estilos-column.css') }}">
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Head-->
      <header class="page-head">
        <!-- RD Navbar Transparent-->
        @include('navbar-contacto')
        <section class="context-dark">
          <div class="parallax-container" data-parallax-img="images/bg-01-1920x795.jpg">
            <div class="parallax-content">
              <div class="shell section-top-34 section-sm-top-98 section-bottom-34">
                <div>
                  <h1 class="text-bold">Contacto</h1>
                </div>
                
              </div>
            </div>
          </div>
        </section>
      </header>
      <!-- Page Contents-->
      <main class="page-content">
        <section class="section-98 section-sm-110">
          <div class="shell">
            {{--<h3 class="text-bold"> Rosa Flaqué (Especialista de Proyectos)</h3>
            <hr class="divider bg-saffron">--}}
            <div class="offset-sm-top-66">
              <div class="range">
                <div class="cell-sm-4">

                  <div class="offset-top-34" style="margin-top: 110px;">
                    <address class="contact-info text-md-left">
                      <ul class="list-unstyled p">
                        <li class="reveal-block"><span class="icon icon-xxs text-middle"></span> <a class="reveal-inline-block text-middle" href="tel:1-800-7650-986"><strong>Rosa Flaqué (Especialista de Proyectos)</strong><br> Espail Llar</a>
                        </li>
                        <li class="reveal-block"><span class="icon icon-xxs mdi mdi-map-marker text-middle"></span> <a class="reveal-inline-block text-middle" href="tel:1-800-7650-986">Carrer Cronista Sess&eacute;, 5 43302 Tarragona</a>
                        </li>
                        <li class="reveal-block"><span class="icon icon-xxs mdi mdi-phone text-middle"></span> Teléfono:<a class="reveal-inline-block text-middle" href="tel:+34655558447" style="color:#15978A;"><strong>+34 655 558 447</strong></a>
                        </li>
                        <li class="reveal-block"><span class="icon icon-xxs mdi mdi-email-open text-middle"></span>E-mail: <a class="reveal-inline-block text-middle" href="mailto:administracio@espaillar.esg" style="color:#15978A;"><strong>administracio@espaillar.es</strong></a>
                        </li>
                      </ul>
                    </address>
                  </div>
                </div>
                <div class="cell-sm-8">
                  <div class="inset-md-left-50">
                    {{--<div class="row">
                      <div class="col-xs-6 col-md-4">
                        <div class="unit unit-sm-horizontal unit-spacing-xs">
                          <div class="unit-left"><span class="icon icon-sm mdi mdi-clipboard-check text-mantis"></span></div>
                          <div class="unit-body text-sm-left">
                            <div>
                              <h2 class="text-bold">582</h2>
                            </div>
                            <p class="text-light offset-top-0"><span class="big text-uppercase">recent deals</span></p>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-4">
                        <div class="unit unit-sm-horizontal unit-spacing-xs">
                          <div class="unit-left"><span class="icon icon-sm mdi mdi-domain text-malibu"></span></div>
                          <div class="unit-body text-sm-left">
                            <div>
                              <h2 class="text-bold">1844</h2>
                            </div>
                            <p class="text-light offset-top-0"><span class="big text-uppercase">properties</span></p>
                          </div>
                        </div>
                      </div>
                    </div>--}}
                    <div class="text-left offset-top-50">
                      <h2 class="text-bold" style="color:#15879A">Solicita Informaci&oacute;n: 655 558 447 </h2>
                      <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                    <div class="range range-xs-center">
                      <div class="cell-sm-12">
                        <div class="form-group">
                          <label class="form-label" for="mailform-first-name">Nombre</label>
                          <input class="form-control bg-white" id="mailform-first-name" type="text" name="first name" data-constraints="@Required">
                        </div>
                      </div>
                      <div class="cell-sm-12">
                        <div class="form-group offset-sm-top-30">
                          <label class="form-label" for="mailform-email">Correo Electronico</label>
                          <input class="form-control bg-white" id="mailform-email" type="text" name="email" data-constraints="@Email @Required">
                        </div>
                      </div>
                      <div class="cell-sm-12">
                        <div class="form-group offset-sm-top-30">
                          <label class="form-label" for="mailform-message">Mensaje</label>
                          <textarea class="form-control bg-white" id="mailform-message" name="message" data-constraints="@Required"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="offset-top-24 text-center">
                      <button class="btn btn-primary" type="submit">Enviar</button>
                    </div>
                  </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--Section Get In Touch-->
      
      </main>
      <!-- Page Footer-->
      <!-- Default footer-->
    
    </div>
     @include('footer')
    <!-- Java script-->
    <script src="{{ asset('js/core.min.js')}}"></script>
    <script src="{{ asset('js/script.js')}}"></script>
  </body>
</html>
<section class="section-34 estilo-1">
  <div class="shell-fluid" id="main">
    <div class="fusion-text fusion-text-1">
      <h1  class="fusion-responsive-typography-calculated ">
        <span style="color: #15879A;">¿Has pensado en todas las <strong>oportunidades de venta</strong> que perderá tu casa si no le haces un Home Staging?
        </span>
      </h1>
    </div>
  </div>
  <div class="shell-fluid" id="main_a">
    <div class="fusion-text fusion-text-2"><h3 style="text-align: center; line-height: 1.1;  font-size::30px;" class="fusion-responsive-typography-calculated"><strong>Home Staging</strong> es una técnica probada de márketing inmobiliario. Consiste en poner bonita la vivienda para que se venda o alquile rápido y al mejor precio.</h3>
    </div>
  </div>
  <div class="shell-fluid" id="main_b">
    <div class="fusion-text fusion-text-3"><p > Párate a mirar los anuncios de los portales inmobiliarios, descubrirás que la mayoría no llaman la atención de los compradores. ¿Quieres que a tu casa le ocurra lo mismo? <span style="color: #000; font-weight: 500;">Lo peor que te puede pasar, es que tu anuncio inmobiliario se haga viejo en un portal sin que tu casa haya recibido ni una visita.</span></p>
    </div>
  </div>
</section>

<section class="context-dark" >
          <div >
            <div style="background-color:#fff !important">
              <div class="shell section-98 section-sm-110" style="padding-top: 0 !important;">
                <h2 class="text-bold text-center" style="color:#15879A;">Solicita Informaci&oacute;n - 655 558 447</h2>
                <hr class="divider bg-saffron">
                <div class="range text-left offset-top-66">
                  <div class="cell-sm-6 text-left" style="color:#000;">
                      <p><strong>Rosa Flaqu&eacute; (Especialista de Proyectos)</strong><br> Espail Llar</p>
                      <p>Carrer Cronista Sess&eacute;, 5<br> 43302 Tarragona</p>
                      <p>Tel&eacute;fono: +34 655 558 447</p>
                      <p>E-mail:administracio@espaillar.es</p>
                   
                  </div>  
                  <div class="cell-sm-6">
                    <!-- RD Mailform-->
                    <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                      <div class="range">
                         <div class="cell-lg-12 offset-top-24">
                          <div class="form-group">
                            <label class="form-label" for="real-estate-first-name">First name:</label>
                            <input class="form-control" id="real-estate-first-name" type="text" name="name" data-constraints="@Required">
                          </div>
                        </div>
                        <div class="cell-lg-12 offset-top-24">
                          <div class="form-group">
                            <label class="form-label" for="real-estate-last-name">Last name:</label>
                            <input class="form-control" id="real-estate-last-name" type="text" name="last_name" data-constraints="@Required">
                          </div>
                        </div><div class="cell-lg-12 offset-top-24">
                          <div class="form-group">
                            <label class="form-label" for="contact-me-message">Message:</label>
                            <textarea class="form-control" id="contact-me-message" name="message" data-constraints="@Required" style="height: 150px;"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="offset-top-30 text-center">
                        <button class="btn btn-primary" type="submit">ENVIAR</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
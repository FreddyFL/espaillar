<!DOCTYPE html>
<html class="wide wow-animation smoothscroll scrollTo" lang="en">
  <head>
    <!-- Site Title-->
    <title>ESPAIL-LLAR</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="Real Estate web design multipurpose template">
    <meta name="date" content="Dec 26">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat:400,700%7CLato:400,700'">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/estilos-column.css') }}">
		 <style>
      .fullscreen-modal .modal-dialog {
        margin: 0;
        margin-right: auto;
        margin-left: auto;
        width: 100%;
      }
        @media  (min-width: 320px) {
        .pc{
          display: none;
        }
        .mobile{
          display: block;
        } 
        .imagenes{
          width: 250px;
        }
      }
        @media  (min-width: 360px) {
        .pc{
          display: none;
        }
        .mobile{
          display: block;
        } 
        .imagenes{
          width: 250px;
        }
      }
      @media  (min-width: 393px) {
        .pc{
          display: none;
        }
        .mobile{
          display: block;
        } 
        .imagenes{
          width: 250px;
        }
      }
      @media  (min-width: 425px) {
        .pc{
          display: none;
        }
        .mobile{
          display: block;
        } 
        .imagenes{
          width: 250px;
        }
      }
        @media  (min-width: 540px) {
        .pc{
          display: none;
        }
        .mobile{
          display: block;
        } 
        .imagenes{
          width: 250px;
        }
      }
      @media  (min-width: 800px) {
       .pc{
          display: none;
        }
        .mobile{
          display: block;
        } 
        .imagenes{
          width: 400px;
        }
      }
      @media  (min-width: 764px) {
        .pc{
        display: block;
      }
      .mobile{
          display: none;
      }
        .imagenes{
          width: 440px;
        }
      }
      @media  (min-width: 800px) {
        .pc{
        display: block;
      }
      .mobile{
          display: none;
      }
        .imagenes{
          width: 440px;
        }
      }
      @media  (min-width: 912px) {
        .pc{
        display: block;
      }
      .mobile{
          display: none;
      }
        .imagenes{
          width: 440px;
        }
      }
      @media  (min-width: 1024px) {
        .pc{
        display: block;
      }
      .mobile{
          display: none;
      }
        .imagenes{
          width: 440px;
        }
      }
      @media  (min-width: 1290px) {
        .pc{
        display: block;
      }
      .mobile{
          display: none;
      }
        .imagenes{
          width: 500px;
        }
      }
      @media  (min-width: 1440px) {
        .pc{
        display: block;
      }
      .mobile{
          display: none;
      }
        .imagenes{
          width: 570px;
        }
      }
       @media  (min-width: 1920px) {
        .pc{
        display: block;
      }
      .mobile{
          display: none;
      }
        .imagenes{
          width: 600px;
        }
      }
    
      @media (min-width: 768px) {
        .fullscreen-modal .modal-dialog {
          width: 750px;
        }
      }
      @media (min-width: 992px) {
        .fullscreen-modal .modal-dialog {
          width: auto;
        }
      }
      @media (min-width: 1200px) {
        .fullscreen-modal .modal-dialog {
           width: auto;
        }
      }
      @media (min-width: 1920px) {
        .fullscreen-modal .modal-dialog {
           width: auto;
        }
      }
     </style> 
  </head>
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Head-->
      <header class="page-head">
        <!-- RD Navbar Transparent-->
       @include('navbar-proyectos')
        
      </header>
      <!-- Page Contents-->
      <main class="page-content" style="background-color: #fff;">
        <!--Section Property Filter-->
      <section class="context-dark">
          <div class="parallax-container" >
            <div >
              <div class="shell section-top-34 section-sm-top-98 section-bottom-34">
                <div>
                  <h1 class="text-bold text-center" style="color: #15879A;">NUESTROS  PROYECTOS</h1>
                </div>
              
              </div>
            </div>
          </div>
        </section>
        <!--Section Properties-->
        <section class="section-66 section-sm-bottom-110">
          <div class="shell">
            <div class="range range-xs-center">
              <div class="cell-md-12">
                <div class="range">
                  <div class="cell-sm-4 offset-top-66">
                      <h5 class="text-bold text-primary img-puntero"><a data-toggle="modal" data-target="#exampleModal">Carrer Ebre - Hospitalet del Llobregat</a></h5>
                   
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <img class="img-responsive reveal-inline-block img-puntero" src=" images/PisoCarrerEbreHospitalet/CarrerEbre1.jpg" width="370" height="250"  data-toggle="modal" data-target="#exampleModal">
                      </div>
                      
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-66 ">
                    <h5 class="text-bold text-primary img-puntero"><a data-toggle="modal" data-target="#exampleModalPinar">Casa al Pinar - Reus</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <img class="img-responsive reveal-inline-block img-puntero" src="images/CasaPinarReus/PinarReus1.jpg" width="370" height="250" alt="" data-toggle="modal" data-target="#exampleModalPinar" >
                      </div>
                      
                    </div>
                  </div>
                   <div class="cell-sm-4 offset-top-66">
                      <h5 class="text-bold text-primary img-puntero"><a data-toggle="modal" data-target="#CalleBernat">Calle Bernat Metge - Reus</a></h5>
                      
                    <div class="text-sm-left offset-top-24">
                      <div>
                         <img class="img-responsive reveal-inline-block img-puntero" src="images/PisoCalleBernatMetgeReus/BernatMetge1.jpg" width="370" height="250" alt=""  data-toggle="modal" data-target="#CalleBernat">
                      </div>
                     
                    </div>
                  </div>
                   <div class="cell-sm-4 offset-top-66">
                    <h5 class="text-bold text-primary img-puntero"><a data-toggle="modal" data-target="#exampleModal">The Presidio Residences</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <img class="img-responsive reveal-inline-block img-puntero" src="images/home-img-07-370x250.jpg" width="370" height="250" alt=""  data-toggle="modal" data-target="#exampleModal">
                      </div>
                      
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-66">
                    <h5 class="text-bold text-primary img-puntero"><a data-toggle="modal" data-target="#exampleModal">The Presidio Residences</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <img class="img-responsive reveal-inline-block img-puntero" src="images/home-img-07-370x250.jpg" width="370" height="250" alt=""  data-toggle="modal" data-target="#exampleModal">
                      </div>
                    </div>
                  </div>
                   <div class="cell-sm-4 offset-top-66">
                    <h5 class="text-bold text-primary img-puntero"><a data-toggle="modal" data-target="#exampleModal">The Presidio Residences</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <img class="img-responsive reveal-inline-block img-puntero" src="images/home-img-07-370x250.jpg" width="370" height="250" alt=""  data-toggle="modal" data-target="#exampleModal">
                      </div>
                      
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-66">
                    <h5 class="text-bold text-primary img-puntero"><a data-toggle="modal" data-target="#exampleModal">South Bay Residence</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        <img class="img-responsive reveal-inline-block img-puntero" src="images/home-img-08-370x250.jpg" width="370" height="250" alt=""  
                        data-toggle="modal" data-target="#exampleModal">
                      </div>
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-66">
                      <h5 class="text-bold text-primary img-puntero"><a data-toggle="modal" data-target="#exampleModal">Avalon at Mission Bay</a></h5> 
                      <div class="text-sm-left offset-top-24">
                      <div>
                         <img class="img-responsive reveal-inline-block img-puntero" src="images/home-img-09-370x250.jpg" width="370" height="250" alt=""  
                         data-toggle="modal" data-target="#exampleModal">
                      </div>
                    </div>
                  </div>
                  <div class="cell-sm-4 offset-top-66">
                    <h5 class="text-bold text-primary img-puntero"><a data-toggle="modal" data-target="#exampleModal">Bayside Village Apartments</a></h5>
                    <div class="text-sm-left offset-top-24">
                      <div>
                        
                        <img class="img-responsive reveal-inline-block img-puntero" src="images/home-img-10-370x250.jpg" width="370" height="250" alt=""  data-toggle="modal" data-target="#exampleModal">
                      </div>
                    </div>
                  </div>
                </div>
               
              </div>
              
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
      <!-- Default footer-->
    
   
      <!-- Modal -->
      @include('modal_propiedad')
      @include('modal')
      @include('modal_calle_bernant')

    </div>

      @include('footer')
     <script src="{{ asset('js/core.min.js')}}"></script>
    <script src="{{ asset('js/script.js')}}"></script>
  </body>
</html>
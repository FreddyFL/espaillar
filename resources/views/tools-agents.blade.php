<section class="section-98 section-md-110" style="background-color: #fff;
    background-position: center center; background-repeat: no-repeat; padding-top: 100px;
    padding-right: 30px;padding-bottom: 0px; padding-left: 30px; margin-bottom: 100px;
    margin-top: 0px;border-width: 0px 0px 0px 0px;border-color: #eae9e9;border-style: solid;">
  <div class="shell" >
    <h2 style="text-align: center; font-size:44; line-height: 1.1;" data-fontsize="44" data-lineheight="48.4px" class="fusion-responsive-typography-calculated"><span style="color:#15879A;">Quiero saber más<br>
<a style="color: #ffffff;" href="tel:675 917 328"><strong style="color:#15879A;">675 917 328</strong></a></span></h2>
  </div>
  <div class="fusion-aligncenter"><style>.fusion-button.button-1 .fusion-button-text,.fusion-button.button-1 i{color:#15879A;}.fusion-button.button-1{border-color:#15879A;border-radius:0px 0px 0px 0px;background:rgba(255,255,255,0);}.fusion-button.button-1:hover .fusion-button-text,.fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text,.fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text,.fusion-button.button-1:active i{color:#ffffff;}.fusion-button.button-1:hover,.fusion-button.button-1:active,.fusion-button.button-1:focus{border-color:#15879A;background:#15879A;}</style><a class="fusion-button button-flat button-xlarge button-custom button-1 fusion-button-default-span fusion-button-default-type" target="_self" href="https://www.instagram.com/anna_garcia_home_staging/"><i class="fa-instagram fab button-icon-left" aria-hidden="true"></i><span class="fusion-button-text">Síguenos en instagram</span></a></div>
</section>
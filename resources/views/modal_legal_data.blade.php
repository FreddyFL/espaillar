<div class="modal fade" id="modal-legal-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 0px;  z-index: 99999;    position: fixed;
  right: 0;  bottom: 0;  left: 0;  overflow: hidden;">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content" style="border-radius: 0;background-clip: border-box;height: 100%;">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" style="color:#000 !important;">Datos Legales</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #15879A;opacity: 1;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="color:#000 !important;text-align: initial;">
        <p style="margin: 0 0 10px;font-size: 14px;
    line-height: 1.42857;
    color: #333333;">En cumplimiento de la Ley de Servicios de la Sociedad de la Información y de Comercio Electrónico (Ley 34/2002 - LSSI-CE), en vigor desde el 12 de Octubre de 2002, se pone en conocimiento público la siguiente información:</p>
        <p style="font-size: 14px;
    line-height: 1.42857;
    color: #333333;"><strong>DATOS DE CONTACTO</strong></p>
        <ul style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;font-size: 14px; line-height: 1.42857;     color: #333333;    padding-left: 2rem; list-style: disc; display:revert !important;">
          <li style="display:revert !important;">Nombre Fiscal:{{ env('TB_REPRESENTANTE') }}</li>
          <li style="display:revert !important;"> Dirección:{{ env('TB_DOMICILIO_SOCIAL') }} </li>
          <li style="display:revert !important;">Código postal:{{ env('TB_CP') }}</li>
          <li style="display:revert !important;">Email:{{ env('TB_MAIL') }}</li>
          <li style="display:revert !important;">NIF:{{ env('TB_NIF') }} </li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
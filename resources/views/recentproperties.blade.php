
<hr style="border-top: 4px solid #747474;height: 10px;  width: 70%;opacity: 0.4;">
 
<section class="section-bottom-98 section-top-34 section-md-bottom-110 section-md-top-66 estilo-1" style="/*background-image: url(images/home1.jpg);*/ background-color: rgba(255, 255, 255, 0);    background-position: left top;background-repeat: repeat; padding: 0px 10% 120px; margin-bottom: 0px;
    margin-top: 0px; border-width: 0px; border-color: rgb(234, 233, 233); border-style: solid;">
  
    
  <div class="">
 
    <div class="">
      <div class="">
        <h2 style="text-align: center; text-transform: none; font-size:24px; line-height: 1.1;
        color: #747474; /*color:#15879A;font-weight: 500;*/" data-fontsize="34" data-lineheight="37.4px" class="fusion-responsive-typography-calculated">
          <strong>El Home Staging consigue enamorar,</strong><br> hasta a los compradores menos interesados.
        </h2>
      </div>
    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
     
      <i class="mdi mdi-brush button-icon-left" aria-hidden="true" style="color: #15879A;font-size: 24px;"></i>
        <h4 style="font-size:18px;color: #15879A;">CON UNA MANO DE <strong style="color:#15879A;font-weight: 500;">PINTURA</strong> BLANCA ILUMINARÁS EL ESPACIO</h4>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
        <i class="mdi mdi-wrench button-icon-left" aria-hidden="true" style="color: #15879A;font-size: 24px;"></i>
        <h4 style="font-size:18px;color: #15879A;">ALGUNOS <strong style="color: #15879A;font-weight: 500;">ARREGLOS </strong>AYUDARÁN A QUE LOS COMPRADORES NO REBAJEN EL PRECIO</h4>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3"> 
        <i class="mdi mdi-lightbulb button-icon-left" aria-hidden="true" style="color: #15879A;font-size: 24px;"></i>
        <h4 style="font-size:18px;color: #15879A;" >UNA CORRECTA <strong style="color: #15879A;font-weight: 500;">ILUMINACIÓN </strong>PERMITIRÁ QUE SE VEAN TODAS LAS POSIBILIDADES</h4>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
        <i class="mdi mdi-autorenew button-icon-left" aria-hidden="true" style="color: #15879A;font-size: 24px;"></i>
       <h4 style="font-size:18px;color: #15879A;"> SIN TRASTOS POR EN MEDIO <strong style="color: #15879A;font-weight: 500;">CIRCULAREMOS </strong>MEJOR</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
        <i class="mdi mdi-cast button-icon-left" aria-hidden="true" style="color: #15879A;font-size: 24px;"></i>
        <h4 style="font-size:18px;color: #15879A;">UNA <strong style="color: #15879A;font-weight: 500;"> DECORACIÓN NEUTRA Y DESPERSONALIZADA</strong> GUSTARÁ A TODOS</h4>

      </div>
      <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
         <i class="mdi mdi-map button-icon-left" aria-hidden="true" style="color: #15879A;font-size: 24px;"></i>
        <h4 style="font-size:18px;color: #15879A;">LOS <strong style="color:#15879A;font-weight: 500;">PLANOS</strong>PERMITIRÁN VER LA DISTRIBUCIÓN Y LOS ESPACIOS</h4>
      </div>  
      <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
         <i class="mdi mdi-camera button-icon-left" aria-hidden="true" style="color: #15879A;font-size: 24px;"></i>
        <h4 style="font-size:18px;color: #15879A;">LAS <strong style="color: #15879A;font-weight: 500;">FOTOGRAFÍAS PROFESIONALES </strong>CAPTARÁN LA ATENCIÓN DE LOS COMPRADORES</h4>
      </div>  
    </div>
    <hr style="border-top: 4px solid #747474;opacity: 0.4;width: 80%;">
  </div>
</section>
<!--Section Search form-->
        <section class="context-dark">
          <!-- Swiper-->
          <div class="swiper-container swiper-slider" data-height="43%" data-min-height="600px" data-dots="true" data-autoplay="5500">
            <div class="swiper-wrapper">
              <div class="swiper-slide swiper-slide-overlay-disable" data-slide-bg="images/slide-01-1920x640.jpg" style="background-position: center center;"> </div>
              <div class="swiper-slide swiper-slide-overlay-disable" data-slide-bg="images/slide-02-1920x640.jpg" style="background-position: center center;"></div>
              <div class="swiper-slide swiper-slide-overlay-disable" data-slide-bg="images/slide-03-1920x640.jpg" style="background-position: center center;"></div>
            </div>
            <div class="swiper-caption-wraper">
              <div class="shell section-41">
                <div class="range range-xs-center">
                  <h1 class="text-bold">Professional realtor works for you</h1>
                  {{--<div class="cell-xs-10 cell-md-7 cell-lg-6">
                    <div class="offset-top-24 offset-md-top-50 text-left">
                      <!-- Responsive-tabs-->
                      <div class="responsive-tabs responsive-tabs-dashed" data-type="horizontal">
                       <ul class="resp-tabs-list tabs-group-default" data-group="tabs-group-default">
                          <li>sale </li>
                          <li>rent</li>
                          <li>commercial</li>
                        </ul>
                        <div class="resp-tabs-container text-left tabs-group-default" data-group="tabs-group-default">
                          <div class="offset-md-top-10">
                            <form>
                              <div class="group-sm group-top">
                                <div class="group-item element-fullwidth" style="max-width: 390px;">
                                  <div class="form-group">
                                    <label class="form-label rd-input-label" for="home-tabs1-search-form-input">Search by City or Address:</label>
                                    <input class="form-control" id="home-tabs1-search-form-input" type="text" name="s" autocomplete="off">
                                  </div>
                                </div>
                                <button class="btn btn-primary element-fullwidth" type="button" style="max-width: 120px;">search</button>
                              </div>
                            </form>
                          </div>
                          <div class="offset-md-top-10">
                            <form>
                              <div class="group-sm group-top">
                                <div class="group-item element-fullwidth" style="max-width: 390px;">
                                  <div class="form-group">
                                    <label class="form-label rd-input-label" for="home-tabs2-search-form-input">Search by  Address or City:</label>
                                    <input class="form-control" id="home-tabs2-search-form-input" type="text" name="s" autocomplete="off">
                                  </div>
                                </div>
                                <button class="btn btn-primary element-fullwidth" type="button" style="max-width: 120px;">search</button>
                              </div>
                            </form>
                          </div>
                          <div class="offset-md-top-10">
                            <form>
                              <div class="group-sm group-top">
                                <div class="group-item element-fullwidth" style="max-width: 390px;">
                                  <div class="form-group">
                                    <label class="form-label rd-input-label" for="home-tabs3-search-form-input">Search by City or Address:</label>
                                    <input class="form-control" id="home-tabs3-search-form-input" type="text" name="s" autocomplete="off">
                                  </div>
                                </div>
                                <button class="btn btn-primary element-fullwidth" type="button" style="max-width: 120px;">search</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>--}}
                </div>
              </div>
            </div>
            <!-- Swiper Pagination-->
            <div class="swiper-pagination swiper-pagination-type-2"></div>
          </div>
        </section>
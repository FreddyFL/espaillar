 <section class="section-98 section-md-110">
          <div class="shell">
            <h2 class="text-bold">Real Estate Agents</h2>
            <hr class="divider bg-saffron">
            <div class="range range-xs-center offset-top-66">
              <div class="cell-sm-6 cell-md-3">
                <div class="member-block-type-6"><img class="img-responsive reveal-inline-block" src="images/users/user-ashley-mason-270x270.jpg" width="270" height="270" alt="">
                  <div class="text-left offset-top-20">
                    <div>
                      <h5 class="text-bold text-primary"><a href="team-member.html">Ashley Mason</a></h5>
                    </div>
                  </div>
                  <address class="contact-info text-left">
                    <ul class="list-unstyled p">
                      <li class="reveal-block"><span class="icon icon-xxs mdi mdi-phone text-middle"></span> <a class="reveal-inline-block text-middle" href="tel:1-800-7650-986">1-800-7650-986</a>
                      </li>
                      <li class="reveal-block"><span class="icon icon-xxs mdi mdi-email-open text-middle"></span> <a class="reveal-inline-block text-middle" href="mailto:ashley@demolink.org">ashley@demolink.org</a>
                      </li>
                    </ul>
                  </address>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-sm-top-0">
                <div class="member-block-type-6"><img class="img-responsive reveal-inline-block" src="images/users/user-russel-myers-270x270.jpg" width="270" height="270" alt="">
                  <div class="text-left offset-top-20">
                    <div>
                      <h5 class="text-bold text-primary"><a href="team-member.html">Russell Myers</a></h5>
                    </div>
                  </div>
                  <address class="contact-info text-left">
                    <ul class="list-unstyled p">
                      <li class="reveal-block"><span class="icon icon-xxs mdi mdi-phone text-middle"></span> <a class="reveal-inline-block text-middle" href="tel:1-800-7650-986">1-800-7650-986</a>
                      </li>
                      <li class="reveal-block"><span class="icon icon-xxs mdi mdi-email-open text-middle"></span> <a class="reveal-inline-block text-middle" href="mailto:russell@demolink.org">russell@demolink.org</a>
                      </li>
                    </ul>
                  </address>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0">
                <div class="member-block-type-6"><img class="img-responsive reveal-inline-block" src="images/users/user-shirley-vasques-270x270.jpg" width="270" height="270" alt="">
                  <div class="text-left offset-top-20">
                    <div>
                      <h5 class="text-bold text-primary"><a href="team-member.html">Shirley Vasquez</a></h5>
                    </div>
                  </div>
                  <address class="contact-info text-left">
                    <ul class="list-unstyled p">
                      <li class="reveal-block"><span class="icon icon-xxs mdi mdi-phone text-middle"></span> <a class="reveal-inline-block text-middle" href="tel:1-800-7650-986">1-800-7650-986</a>
                      </li>
                      <li class="reveal-block"><span class="icon icon-xxs mdi mdi-email-open text-middle"></span> <a class="reveal-inline-block text-middle" href="mailto:shirley@demolink.org">shirley@demolink.org</a>
                      </li>
                    </ul>
                  </address>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0">
                <div class="member-block-type-6"><img class="img-responsive reveal-inline-block" src="images/users/user-terry-sandoval-270x270.jpg" width="270" height="270" alt="">
                  <div class="text-left offset-top-20">
                    <div>
                      <h5 class="text-bold text-primary"><a href="team-member.html">Terry Sandoval</a></h5>
                    </div>
                  </div>
                  <address class="contact-info text-left">
                    <ul class="list-unstyled p">
                      <li><span class="icon icon-xxs mdi mdi-phone text-middle"></span> <a class="reveal-inline-block text-middle" href="tel:1-800-7650-986">1-800-7650-986</a>
                      </li>
                      <li><span class="icon icon-xxs mdi mdi-email-open text-middle"></span> <a class="reveal-inline-block text-middle" href="mailto:terry@demolink.org">terry@demolink.org</a>
                      </li>
                    </ul>
                  </address>
                </div>
              </div>
            </div>
            <div class="offset-top-66"><a class="btn btn-primary" href="about.html">view all agents</a></div>
          </div>
        </section>
<!DOCTYPE html>
<html class="wide wow-animation smoothscroll scrollTo" lang="en">
  <head>
    <!-- Site Title-->
    <title>ESPAIL-LLAR</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="Real Estate web design multipurpose template">
    <meta name="date" content="Dec 26">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat:400,700%7CLato:400,700'">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/espillar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/estilos-column.css') }}">
    
        <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
  </head>
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Head-->
      <header class="page-head">
       @include('navbar')
      </header>
      <!-- Page Contents-->
      <main class="page-content">
        <!--Section Search form-->
        @include('searchform')
        <!--Section thumbnails terry-->
        @include('thumbnails')
         <!--Section Testimonials-->
        @include('testimonials')
        <!--Section Recent Properties-->
        @include('recentproperties')
        <!--Section Real Estate Tools and Resources-->
        @include('tools')
         
        @include('finalrealstatetools')
        
      </main>
      <!-- Page Footers-->
      <!-- Default footer-->
    
    </div>
      @include('footer')
        
    <!-- Global Mailform Output
    <div class="snackbars" id="form-output-global"></div>-->
    <!-- PhotoSwipe Gallery-->
    @include('photoswipegallery')
    <!-- Java script-->
    <script src="{{ asset('js/core.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
   
    
  </body>
</html>
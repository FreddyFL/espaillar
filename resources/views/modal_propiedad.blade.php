<div class="modal fullscreen-modal  fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 0px;  z-index: 99999;    position: fixed;
  right: 0;  bottom: 0;  left: 0;  overflow-y: auto;">
  <div class="modal-dialog" style="position: fixed;
  margin: 0;
  width: 100%;
  height: 100%;
  padding: 0;">
    <div class="modal-content" style="border-radius: 0;background-clip: border-box;height: 100%;">
      <div class="modal-header" style="border-bottom: 1px solid #fff;">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #15879A;opacity: 1;">
          <span aria-hidden="true" style="font-size: 45px;">&times;</span>
        </button>
      </div>
      <div class="modal-body pc" style="padding: 0px;height: 912px;  width: 100%;overflow-y: auto;">
        <section class="section-98 section-sm-110">
          <div class="shell">
            <h2 class="text-bold text-center">Piso Carrer Ebre - Hospitalet del Llobregat</h2>
            <hr class="divider bg-saffron">
            <div class="offset-sm-top-66">
              <div class="range">
                 <div class="cell-md-6 cell-lg-6 cell-xl-6">
                  <div class="text-sm-left offset-top-50">
                   <h5 class="text-bold">Descripción</h5>
                    <p>Piso en Carrer Ebre de Hospitalet del Llobregat.</p>
					<p>Piso actualmente amueblado i habitado.</p>
                  </div>
                  </div>
                <div class="cell-md-6 cell-lg-6 cell-xl-6">
                  <!-- Owl Carousel-->
                  <div class="owl-carousel owl-carousel-classic imagenes" data-items="1" data-dots="true" data-nav="true" data-autoplay="true" data-loop="true" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]" data-photo-swipe-gallery=""  style="margin: 0 auto;">
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre1.jpg">
                      <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre1.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre2.jpg">
                      <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre2.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre3.jpg">
                      <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre3.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre4.jpg">
                      <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre4.jpg" alt=""/>
                      </figure>
                    </a>
                   <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre5.jpg">
                      <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre5.jpg" alt=""/>
                      </figure>
                    </a>
                      <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre6.jpg">
                      <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre6.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre7.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre7.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre8.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre8.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre9.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre9.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre10.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre10.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre11.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre11.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre12.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre12.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre13.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre13.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre14.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre14.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre15.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre15.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre16.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre16.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre17.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre17.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre18.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre18.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre19.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre19.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre20.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre20.jpg" alt=""/>
                      </figure>
                    </a>
                  </div>
                   </div>

                 
                 </div>
            </div>
          </div>
        </section>
      </div>
      <div class="modal-body mobile" style="padding: 0px;height: 489px;  width: 100%;  overflow-y: auto;">
        <section class="section-98 section-sm-110">
          <div class="shell">
            <h2 class="text-bold text-center">The Presidio Residences</h2>
            <hr class="divider bg-saffron">
            <div class="offset-sm-top-66">
              <div class="range">
                 
                <div class="cell-md-6 cell-lg-6 cell-xl-6 text-center">
                  <!-- Owl Carousel-->
                  <div class="owl-carousel owl-carousel-classic imagenes" data-items="1" data-dots="true" data-nav="true" data-autoplay="true" data-loop="true" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]" data-photo-swipe-gallery=""  style="margin: 0 auto;">
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-01-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-01-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-02-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-02-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-03-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-03-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-05-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-05-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                   <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-06-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-06-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                      <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-07-770x510.jpg">
                      <figure><img width="770" height="510" src="images/single-property-page-07-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/single-property-page-08-770x510.jpg">
                    <figure><img width="770" height="510" src="images/single-property-page-08-770x510.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre8.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre8.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre9.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre9.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre10.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre10.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre11.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre11.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre12.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre12.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre13.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre13.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre14.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre14.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre15.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre15.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre16.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre16.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre17.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre17.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre18.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre18.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre19.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre19.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCarrerEbreHospitalet/CarrerEbre20.jpg">
                    <figure><img width="770" height="510" src="images/PisoCarrerEbreHospitalet/CarrerEbre20.jpg" alt=""/>
                      </figure>
                    </a>
                  </div>
                   </div>
<div class="cell-md-6 cell-lg-6 cell-xl-6">
                  <div class="text-sm-left offset-top-50">
                  <h5 class="text-bold">Descripción</h5>
                    <p>Piso en Carrer Ebre de Hospitalet del Llobregat.</p>
          <p>Piso actualmente amueblado i habitado.</p>
                    
                  </div>
                  </div>
                 
                 </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
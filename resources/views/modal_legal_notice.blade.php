<div class="modal fade" id="modal-legal-notice" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 0px;  z-index: 99999;    position: fixed;
  right: 0;  bottom: 0;  left: 0;   overflow-y: auto;">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content" style="border-radius: 0;background-clip: border-box;height: 100%;">
           <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" style="color:#000 !important;">AVISO LEGAL, POLITICA DE PRIVACIDAD Y PROTECCION DE DATOS</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #15879A;opacity: 1;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="font-size: 14px;line-height: 1.42857;text-align: initial;">
        <h5 style="color:#000 !important;"><strong>AVISO LEGAL.</strong></h5>
        <p style="color:#000 !important;">En cumplimiento del artículo 10 de la Ley  34/2002, de 12 de Octubre, de Servicios de la Sociedad de la Información y de Comercio electrónico {{ env('TB_REPRESENTANTE') }}  pone a disposición de los usuarios de este sitio web la siguiente información:</p>
        <p style="color:#000 !important;">El sitio web  {{ env('TB_SITE_WEB') }}, es propiedad de {{ env('TB_REPRESENTANTE') }}, con domicilio social en  {{ env('TB_DOMICILIO_SOCIAL') }} .</p>
        <p style="color:#000 !important;">Usted puede contactarnos a través de los siguientes medios de comunicación:</p>
        <ul style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;font-size: 14px; line-height: 1.42857;     color: #333333;    padding-left: 2rem; list-style: disc; display:revert !important;">
          <li style="display:revert !important;">Telefono:{{ env('TB_TELEFONO') }}</li>
          <li style="display:revert !important;">Correo electrónico: {{ env('TB_MAIL') }}</li>
        </ul>
        <br>
        <h5 style="color:#000 !important;"><strong>OBJETO:</strong></h5>
        <p style="color:#000 !important;">La presente página Web ha sido diseñada para dar a conocer los servicios ofertados por la entidad, así como para la recepción de posibles clientes.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>PROPIEDAD INTELECTUAL E INDUSTRIAL:</strong></h5>
        <p style="color:#000 !important;">Los derechos de propiedad intelectual de la página  www.tecnobravo.com, su código fuente, diseño, estructuras de navegación y los distintos elementos en ella contenidos son titularidad de {{ env('TB_REPRESENTANTE') }}, a quien corresponde el ejercicio exclusivo de los derechos de explotación de los mismos en cualquier forma y, en especial, los derechos de reproducción, distribución, comunicación pública y transformación, de acuerdo con la legislación española y de la unión europea aplicable.</p>
        <p style="color:#000 !important;">El portal de {{ env('TB_SITE_WEB') }}, las páginas que comprende y la información o elementos contenidos en las mismas, incluyen textos, documentos, fotografías, dibujos, representaciones gráficas, programas informáticos, así como logotipos, marcas, nombres comerciales, u otros signos distintivos, protegidos por derechos de propiedad intelectual o industrial, de los que {{ env('TB_REPRESENTANTE') }} son titulares o legítimas licenciatarias.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>CONTENIDOS:</strong></h5>
        <p style="color:#000 !important;">Se facilita a través de esta Web información acerca de servicios destinados a conocimiento público que en todo caso se sujetarán a los términos y condiciones expresamente detallados en cada momento y que son accesibles desde esta página Web, los cuales se sujetarán a las distintas disposiciones legales de aplicación.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>ACCESO Y USO:</strong></h5>
        <p style="color:#000 !important;">Tanto el acceso a esta página Web, como el uso que pueda hacerse de la información y contenidos incluidos en la misma, será de la exclusiva responsabilidad de quien lo realice.</p>
        <p style="color:#000 !important;">Las condiciones de acceso a este Web estarán supeditadas a la legalidad vigente y los principios de la buena fe y uso lícito por parte del usuario de la misma, quedando prohibido con carácter general cualquier tipo de actuación en perjuicio de {{ env('TB_REPRESENTANTE') }}.</p>
        <p style="color:#000 !important;">Se considerará terminantemente prohibido el uso de la presente página Web con fines ilegales o no autorizados.</p>
        <p style="color:#000 !important;">Queda prohibida cualquier modalidad de explotación, incluyendo todo tipo de reproducción, distribución, cesión a terceros, comunicación pública y transformación, mediante cualquier tipo de soporte y medio, de las obras antes referidas, creaciones y signos distintivos sin autorización previa y expresa de sus respectivos titulares. El incumplimiento de esta prohibición podrá constituir infracción sancionable por la legislación vigente.</p>
        <p style="color:#000 !important;">No obstante, por su cuenta y riesgo, el usuario podrá descargar o realizar copia de tales elementos exclusivamente para su uso personal, siempre que no infrinja ninguno de los derechos de propiedad intelectual o industrial de {{ env('TB_REPRESENTANTE') }}, ni los altere total o parcialmente. En ningún caso, ello significará una autorización o licencia sobre los derechos de propiedad de {{ env('TB_REPRESENTANTE') }}.</p>
        <p style="color:#000 !important;">Queda prohibido, salvo en los casos que expresamente lo autorice {{ env('TB_REPRESENTANTE') }}, presentar las páginas de {{ env('TB_REPRESENTANTE') }}, o la información contenida en ellas bajo frames o marcos, signos distintivos, marcas o denominaciones sociales o comerciales de otra persona, empresa o entidad.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>RESPONSABILIDAD:</strong></h5>
        <p style="color:#000 !important;">{{ env('TB_REPRESENTANTE') }} no se hace responsable bajo ningún concepto por ningún tipo de daño que pudiesen ocasionar los Usuarios a la presente página Web, o a cualquier otra, por el uso ilegal o indebido de la misma, o de los contenidos e informaciones accesibles o facilitadas a través de ella.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>SERVICIO:</strong></h5>
        <p style="color:#000 !important;">{{ env('TB_REPRESENTANTE') }} se reserva el derecho de suspender el acceso a su página Web, sin previo aviso, de forma discrecional y temporal, por razones técnicas o de cualquier otra índole, pudiendo asimismo modificar unilateralmente tanto las condiciones de acceso, como la totalidad o parte de los contenidos en ella incluidos.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>GENERALES:</strong></h5>
        <p style="color:#000 !important;">Para toda cuestión litigiosa o que incumba a la Página Web de {{ env('TB_REPRESENTANTE') }}, será de aplicación la legislación española, siendo competentes para la resolución de todos los conflictos derivados o relacionados con el uso de esta página Web, los Juzgados y Tribunales del domicilio del usuario. El acceso a la página Web de {{ env('TB_REPRESENTANTE') }} implica la aceptación de todas las condiciones anteriormente expresadas.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>HIPERENLACES:</strong></h5>
        <p style="color:#000 !important;">Los hiperenlaces contenidos en el sitio Web de {{ env('TB_REPRESENTANTE') }} pueden dirigir a páginas Web de terceros. {{ env('TB_REPRESENTANTE') }} no asume ninguna responsabilidad por el contenido, informaciones o servicios que pudieran aparecer en dichos sitios, que tendrán exclusivamente carácter informativo y que en ningún caso implican relación alguna entre {{ env('TB_REPRESENTANTE') }} y a las personas o entidades titulares de tales contenidos o titulares de los sitios donde se encuentren.</p>
        <br>
        <hr>
        <h4 style="color:#000 !important;">POLITICA DE PRIVACIDAD Y PROTECCIÓN DE DATOS.</h4>
        <br>
        <br>
        <h5 style="color:#000 !important;"><strong>GENERAL:</strong></h5>
        <p style="color:#000 !important;">A los efectos de lo previsto en la Ley Orgánica 15/1999, de 13 de Diciembre, de Protección de Datos de Carácter Personal, {{ env('TB_REPRESENTANTE') }}, le informa que cumple íntegramente con la legislación vigente en materia de protección de datos de carácter personal, y con los compromisos de confidencialidad propios de su actividad.</p>
        <p style="color:#000 !important;">{{ env('TB_REPRESENTANTE') }}, pone en su conocimiento la existencia de un ficheros automatizado de datos de carácter personal, titularidad de {{ env('TB_REPRESENTANTE') }}, para las finalidades propias de gestión, comunicación e información. El citado fichero se encuentra inscrito en el Registro General de la Agencia Española de Protección de Datos, al que puede acceder el usuario para comprobar la situación de los mismos.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>MEDIDAS Y NIVELES DE SEGURIDAD:</strong></h5>
        <p style="color:#000 !important;">{{ env('TB_REPRESENTANTE') }}, ha adoptado las medidas necesarias para mantener el nivel de seguridad requerido, según la naturaleza de los datos personales tratados y las circunstancias del tratamiento, con el objeto de evitar, en la medida de lo posible y siempre según el estado de la técnica, su alteración, pérdida, tratamiento o acceso no autorizado.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>AMBITO DE APLICACIÓN:</strong></h5>
        <p style="color:#000 !important;">La estructura de ficheros, equipos y sistemas de información con el objeto de dar cumplimiento a la legislación vigente en  materia de protección de datos, se aplicarán a todos los ficheros, temporales o permanentes, titularidad de {{ env('TB_REPRESENTANTE') }}, que contengan datos de carácter personal, así como a cualquier equipo o sistema de información que los trate.</p>
        <p style="color:#000 !important;">Todo el personal contratado por {{ env('TB_REPRESENTANTE') }} y sus Encargados de Tratamiento, están obligados al cumplimiento de la citada normativa, con especial atención en lo relativo a sus funciones y obligaciones, que serán debidamente determinadas por {{ env('TB_REPRESENTANTE') }}.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>RECOGIDA DE DATOS:</strong></h5>
        <p style="color:#000 !important;">La aceptación de las presentes condiciones, precisa del usuario la recogida de unos datos imprescindibles para la prestación de sus servicios, que le serán solicitados personalmente a través de formularios o de la página Web. En el momento de la recogida de los datos, el usuario será debidamente informado de los derechos que le asisten.</p>
        <p style="color:#000 !important;">Para que la información que contienen nuestros ficheros esté siempre actualizada y no contenga errores, pedimos a nuestros clientes y usuarios que nos comuniquen, a la mayor brevedad posible, las modificaciones y rectificaciones de sus datos de carácter personal.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>EJERCICIO DE DERECHOS:</strong></h5>
        <p style="color:#000 !important;">Los derechos de acceso, rectificación, cancelación y oposición, podrán ser ejercitados por el usuario, o quien a este represente, mediante solicitud escrita y firmada, dirigida a {{ env('TB_REPRESENTANTE') }}, {{ env('TB_DOMICILIO_SOCIAL') }}, con Código Postal {{ env('TB_CP') }}, España. No obstante podrán utilizarse otros medios que permitan reconocer la identidad del cliente que ejercite cualquiera de los anteriores derechos.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>CONSENTIMIENTO:</strong></h5>
        <p style="color:#000 !important;">El usuario prestará su consentimiento para que {{ env('TB_REPRESENTANTE') }} pueda hacer uso de sus datos personales a fin de prestar un correcto cumplimiento de los servicios contratados.</p>
        <p style="color:#000 !important;">La cumplimentación del formulario incluido en el sitio o el envío de correos electrónicos u otras comunicaciones a {{ env('TB_REPRESENTANTE') }}, implica el consentimiento expreso del cliente a la inclusión de sus datos de carácter personal en el referido fichero automatizado, titularidad de {{ env('TB_REPRESENTANTE') }}.</p>
        <p style="color:#000 !important;">Al tiempo de la petición de esta información, se comunicará al cliente o usuario del destinatario de la información, de la finalidad para la cual se recogen los datos, de la identidad y dirección del Responsable del Fichero y de la facultad del usuario de ejercitar los derechos de acceso, rectificación, cancelación y oposición al tratamiento de sus datos.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>CESIÓN A TERCEROS:</strong></h5>
        <p style="color:#000 !important;">{{ env('TB_REPRESENTANTE') }} no cede datos de carácter personal sin el consentimiento expreso de sus titulares, que deberá ser concedido en cada ocasión, siendo sólo cedidos con la finalidad expresada y siempre con el consentimiento del usuario o cliente.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>CONFIDENCIALIDAD Y SECRETO PROFESIONAL:</strong></h5>
        <p style="color:#000 !important;">Los datos recogidos en todas las comunicaciones privadas entre {{ env('TB_REPRESENTANTE') }} y los clientes o usuarios serán tratados con absoluta confidencialidad, comprometiéndose {{ env('TB_REPRESENTANTE') }} a la obligación de secreto de los datos de carácter personal, a su deber de guardarlos y adoptar todas las medidas necesarias que eviten su alteración, pérdida y tratamiento o acceso no autorizado, de acuerdo con lo establecido en el Reglamento de Medidas de Seguridad de los Ficheros Automatizados que contengan datos de carácter personal.</p>
        <p style="color:#000 !important;">Además, también tendrá la condición de confidencial la información de cualquier tipo que las partes intercambien entre sí, aquella que estas acuerden que tiene tal naturaleza, o la que simplemente verse sobre el contenido de dicha información. La visualización de datos a través de Internet, no supondrá el acceso directo a los mismos, salvo consentimiento expreso de su titular para cada ocasión.</p>
        <p style="color:#000 !important;">Recomendamos al cliente que no facilite a tercero alguno su identificación, contraseña o números de referencia que {{ env('TB_REPRESENTANTE') }} pudiera proporcionarle. Asimismo, para garantizar que la protección del secreto profesional entre {{ env('TB_REPRESENTANTE') }} y el cliente se preserve en todas las comunicaciones, el cliente/usuario no debe revelar la información confidencial a terceros.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>USO DE COOKIES:</strong></h5>
        <p style="color:#000 !important;">{{ env('TB_REPRESENTANTE') }} puede utilizar cookies cuando un Usuario navegue por las páginas web del Portal. Las cookies se asocian únicamente con el navegador de un ordenador determinado (un Usuario anónimo), y no proporcionan por sí mismas el nombre y apellidos del Usuario. Gracias a las cookies, resulta posible que {{ env('TB_REPRESENTANTE') }} reconozca las preferencias sobre los contenidos del Portal seleccionados en visitas anteriores por los Usuarios, para poder recordar sus preferencias sin que tengan que volver a seleccionarlas en visitas posteriores.</p>
        <p style="color:#000 !important;">El Usuario tiene la posibilidad de configurar su navegador para ser avisado en pantalla de la recepción de cookies y para impedir la instalación de cookies en su disco duro.</p>
        <p style="color:#000 !important;">Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta información. Para utilizar el Portal, no resulta necesario que el Usuario permita la instalación de las cookies enviadas por {{ env('TB_REPRESENTANTE') }}, sin perjuicio de que, en tal caso, sea necesario que el Usuario tenga que realizar esta selección en cada ocasión que visite el Portal.</p>
        <br>
        <h5 style="color:#000 !important;"><strong>CAMBIOS EN LA POLÍTICA DE SEGURIDAD Y DE PROTECCIÓN DE DATOS:</strong></h5>
        <p style="color:#000 !important;">{{ env('TB_REPRESENTANTE') }} se reserva el derecho de modificar su política de seguridad y protección de datos con el objeto de adaptarlo a las novedades legislativas o jurisprudenciales, así como las que pudieran derivarse de códigos tipo existentes en la materia, o por decisiones corporativas estratégicas, con efectos de la fecha de publicación de dicha modificación.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
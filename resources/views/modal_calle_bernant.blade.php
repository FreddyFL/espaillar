<div class="modal fullscreen-modal  fade" id="CalleBernat" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 0px;  z-index: 99999;    position: fixed;
  right: 0;  bottom: 0;  left: 0;  overflow-y: auto;">
  <div class="modal-dialog" style="position: fixed;
  margin: 0;
  width: 100%;
  height: 100%;
  padding: 0;">
    <div class="modal-content" style="border-radius: 0;background-clip: border-box;height: 100%;">
      <div class="modal-header" style="border-bottom: 1px solid #fff;">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #15879A;opacity: 1;">
          <span aria-hidden="true" style="font-size: 45px;">&times;</span>
        </button>
      </div>
      <div class="modal-body pc" style="padding: 0px;height: 912px;  width: 100%;overflow-y: auto;">
        <section class="section-98 section-sm-110">
          <div class="shell">
            <h2 class="text-bold text-center">Calle Bernat Metge - Reus</h2>
            <hr class="divider bg-saffron">
            <div class="offset-sm-top-66">
              <div class="range">
                 <div class="cell-md-4 cell-lg-4 cell-xl-4">
                  <div class="text-sm-left offset-top-50">
                   <h5 class="text-bold">Descripción</h5>
                    <p>Calle Bernat Metge - Reus.</p>
          <p>Piso actualmente amueblado i habitado.</p>
                  </div>
                  </div>
                <div class="cell-md-8 cell-lg-8 cell-xl-8">
                  <!-- Owl Carousel-->
                  <div class="owl-carousel owl-carousel-classic imagenes" data-items="1" data-dots="true" data-nav="true" data-autoplay="true" data-loop="true" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]" data-photo-swipe-gallery=""  style="margin: 0 auto;">
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge1.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge1.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge2.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge2.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge3.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge3.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge4.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge4.jpg" alt=""/>
                      </figure>
                    </a>
                   <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge5.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge5.jpg" alt=""/>
                      </figure>
                    </a>
                      <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge6.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge6.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge7.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge7.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge8.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge8.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge9.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge9.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge10.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge10.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge11.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge11.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge12.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge12.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge13.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge13.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge14.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge14.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge15.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge15.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge16.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge16.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge17.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge17.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge18.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge18.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge19.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge19.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge20.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge20.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge21.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge21.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge22.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge22.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge23.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge23.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge24.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge24.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge25.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge25.jpg" alt=""/>
                      </figure>
                    </a>
                  </div>
                   </div>

                 
                 </div>
            </div>
          </div>
        </section>
      </div>
      <div class="modal-body mobile" style="padding: 0px;height: 489px;  width: 100%;  overflow-y: auto;">
        <section class="section-98 section-sm-110">
          <div class="shell">
            <h2 class="text-bold text-center">The Presidio Residences</h2>
            <hr class="divider bg-saffron">
            <div class="offset-sm-top-66">
              <div class="range">
                 
                <div class="cell-md-6 cell-lg-6 cell-xl-6 text-center">
                  <!-- Owl Carousel-->
                  <div class="owl-carousel owl-carousel-classic imagenes" data-items="1" data-dots="true" data-nav="true" data-autoplay="true" data-loop="true" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]" data-photo-swipe-gallery=""  style="margin: 0 auto;">
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge1.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge1.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge2.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge2.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge3.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge3.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge4.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge4.jpg" alt=""/>
                      </figure>
                    </a>
                   <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge5.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge5.jpg" alt=""/>
                      </figure>
                    </a>
                      <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge6.jpg">
                      <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge6.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge7.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge7.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge8.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge8.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge9.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge9.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge10.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge10.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge11.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge11.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge12.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge12.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge13.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge13.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge14.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge14.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge15.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge15.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge16.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge16.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge17.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge17.jpg" alt=""/>
                      </figure>
                    </a>
                     <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge18.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge18.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge19.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge19.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge20.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge20.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge21.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge21.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge22.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge22.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge23.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge23.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge24.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge24.jpg" alt=""/>
                      </figure>
                    </a>
                    <a class="thumbnail-classic" data-photo-swipe-item="" data-size="770x510" href="images/PisoCalleBernatMetgeReus/BernatMetge25.jpg">
                    <figure><img width="770" height="510" src="images/PisoCalleBernatMetgeReus/BernatMetge25.jpg" alt=""/>
                      </figure>
                    </a>
                  </div>
                   </div>
<div class="cell-md-6 cell-lg-6 cell-xl-6">
                  <div class="text-sm-left offset-top-50">
                    <h5 class="text-bold">Descripción</h5>
                    <p>Calle Bernat Metge - Reus.</p>
          <p>Piso actualmente amueblado i habitado.</p>
                  </div>
                  </div>
                 
                 </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
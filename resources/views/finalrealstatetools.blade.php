<section class="context-dark" style="margin-top: -110px;">
  <div class="parallax-container" data-parallax-img="#">
    <div class="parallax-content" style="background-color:#fff !important ;">
      <div class="shell section-98 section-sm-110">
       {{-- <h2 class="text-bold">Real Estate Tools and Resources</h2>
        <hr class="divider bg-saffron">--}}
        <div class="range range-xs-center offset-top-66">
          <div class="cell-sm-3">
            <!-- Icon Box Type 4--><span class="mdi mdi-home" style="color:#15879A;font-size: 40px;"></span>
            <h5 class="text-bold text-uppercase" style="color:#15879A;">+81%</h5>
            <p class="text-light" style="color:#000;">Alquileres en menos de 15 días.</p>
          </div>
          <div class="cell-sm-3 offset-top-66 offset-sm-top-0">
            <!-- Icon Box Type 4--><span class="mdi mdi-currency-usd" style="color:#15879A;font-size: 40px;"></span>
            <h5 class="text-bold text-uppercase" style="color:#15879A;">+37%</h5>
            <p class="text-light" style="color:#000;">Incrementa el valor de la vivienda.</p>
          </div>
          <div class="cell-sm-3 offset-top-66 offset-sm-top-0">
            <!-- Icon Box Type 4--><span class="mdi mdi-domain" style="color:#15879A;font-size: 40px;"></span>
            <h5 class="text-bold text-uppercase" style="color:#15879A;">+72%</h5>
            <p class="text-light" style="color:#000;">Ventas en menos de 60 días.</p>
          </div>
          <div class="cell-sm-3 offset-top-66 offset-sm-top-0">
            <!-- Icon Box Type 4--><span class="mdi mdi-account-check" style="color:#15879A;font-size: 40px;"></span>
            <h5 class="text-bold text-uppercase" style="color:#15879A;"> +98%</h5>
            <p class="text-light" style="color:#000;">Clientes satisfechos o muy satisfechos.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
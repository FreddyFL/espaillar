<footer class="section-relative section-top-66 section-bottom-34 page-footer bg-gray-base context-dark">
  <div >
    <div class="range-sm-center text-lg-center">
      <div class="cell-sm-8 cell-md-12">
        <div class="range">
        <div class="cell-xs-12 offset-top-66 cell-lg-3 offset-lg-top-0">
            <!-- Footer brand-->
            <div class="footer-brand text-center"><a href="./"><img  src='images/logo.png' alt=''/></a></div>
        </div>
        <div class="cell-xs-12 offset-top-41 cell-md-12 offset-md-top-0 text-center cell-lg-6">
          <h6 class="text-uppercase text-spacing-60"> 
            <p class="mb-0" style="font-size: 12px;">&copy;
              <span class="copyright-year">
                <a href="https://tecnobravo.com" target="_blank"> TecnoBravo</a> &nbsp; | &nbsp;
                <a  href="{{url('/')}}" >Inicio</a>&nbsp; | &nbsp;
                <a  href="{{route('proyectos')}}">Proyecto</a>&nbsp; | &nbsp;
                <a  href="{{route('contactos')}}">Contacto</a>&nbsp; | &nbsp;
                <a  data-toggle="modal" data-target="#modal-legal-notice">Aviso Legal</a>&nbsp; | &nbsp; 
                <a href="#" data-toggle="modal" data-target="#modal-legal-data">Datos Legales</a>
              </span>
            </p>
          </h6>
        </div>
        
        </div>
      </div>
    </div>
  </div>
  <div class="shell text-center offset-top-50">
    <p>
      <small>Web diseñada y albergada por 
        <a href="https://tecnobravo.com" target="_blank">
          <strong>TecnoBravo IT</strong>
        </a>
      </small>  
    </p>
  </div>
</footer>
@include('modal_legal_data')
@include('modal_legal_notice')